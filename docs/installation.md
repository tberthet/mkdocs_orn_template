# Installation

This documentation works with [Mkocs material](https://squidfunk.github.io/mkdocs-material/). It based on markdown files, stocked in docs/ folder.  
Different dependencies and the hierarchy of the documentation are describe in **mkdocs.yml**.

Once your .md files are written and the mkdocs.yml configured, this beautiful website can be built.

## Install requirements

In a virtual environment

``` shell
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt 
```
Or directly in your python
```shell
pip install -r requirements.txt
```

## Run the docs

Once requirements are installed there are 2 possibilities, serve and build.

### Serve
This is very useful when you are writing the doc, this will host the result of your doc with live reloading. You can start it with this command : 
``` 
mkdocs serve

$ INFO     -  Building documentation...
$ INFO     -  Cleaning site directory
$ INFO     -  Documentation built in 0.39 seconds
$ INFO     -  [10:23:55] Serving on http://127.0.0.1:8000/
$ INFO     -  [10:23:57] Browser connected: http://localhost:8000/installation.html

```

And you can access to the website documentation on [http:/localhost:8000/](http://127.0.0.1:8000/)


### Build
The second one is when your are satisfied of your documentation, you can build it to host it on a production server for example.

```
mkdocs build

$ INFO     -  Cleaning site directory
$ INFO     -  Building documentation to directory: /home/berthet/Nancay/Nenufar/RadioGAGA/doc/site
$ INFO     -  Documentation built in 0.40 seconds

```