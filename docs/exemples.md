# Examples

## Some box
!!! info

!!! error

!!! warning "My custom warning"

!!! question
    Put some content in block

??? info "Click here to show content"
    `Hello world`

## Tables 

| n° pin | Name | FPGA pin |
|--------|------|----------|
| b9     | PF4  | 23A      |
| b10    | PF5  | 23B      |
| b11    | PF6  | 24A      |
| b12    | PF7  | 24B      |
| b13    | PE7  | 25A      |
| b14    | PE6  | 25B      |
| b15    | PE5  | 28B      |
| b16    | PE4  | 28A      |

## Code block

``` python title="script.py"

import numpy as np 

print('Hello world')



```

## Content Tabs

=== "Unordered list"

    * Sed sagittis eleifend rutrum
    * Donec vitae suscipit est
    * Nulla tempor lobortis orci

=== "Ordered list"

    1. Sed sagittis eleifend rutrum
    2. Donec vitae suscipit est
    3. Nulla tempor lobortis orci

## Mathjax

$$
\operatorname{ker} f=\{g\in G:f(g)=e_{H}\}{\mbox{.}}
$$

## Checkbox

- [x] Lorem ipsum dolor sit amet, consectetur adipiscing elit
- [ ] Vestibulum convallis sit amet nisi a tincidunt
    * [x] In hac habitasse platea dictumst
    * [x] In scelerisque nibh non dolor mollis congue sed et metus
    * [ ] Praesent sed risus massa
- [ ] Aenean pretium efficitur erat, donec pharetra, ligula non scelerisque
