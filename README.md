# Mkdocs template documentation

This repo is a template to write documentation based on Mkdocs material.  
Doc: https://squidfunk.github.io/mkdocs-material/  
Check the doc, there are many exemples, and it shows how to use different components.

This works with markdown language and it allows to build beautiful and well structured websites documentation.  
Writing documentation in markdown has many advantages : 
- versionable
- close to the code
- sustainable
- Etc ..


## CI/CD

This doc is built on every push with **CI/CD**.  
Build link : [https://tberthet.pages.obspm.fr/mkdocs_orn_template/](https://tberthet.pages.obspm.fr/mkdocs_orn_template/)  


## Clone as template
To start writing new documentation you can clone this repo in your folder as template.

```
git clone https://gitlab.obspm.fr/tberthet/mkdocs_orn_template.git doc
cd doc
rm -rf .git
git init .

```
You can start writing you doc here  
And clean or keep examples  

To develop your doc you can host it locally by running this command : 
```
pip intall -r requirements.txt

mkdocs serve # Host and auto-refresh the website on http://localhost:8000 
# Or
mkdocs build # Create a 'site/' dir with build website
```
Next to deploy your doc on gitlab you have to :   

```
git add .
git commit -m "Initial commit"
# Create your distant repo on https://gitlab.obspm.fr/ and add it as origin
git push -u origin master
```

In Gitlab go to Settings>Pages to see url of your doc, should be like : **https://user.pages.obspm.fr/your_repo_name/**

## Working with ORN SVN

To make it works with SVN you have to set the URL in DOCUMENTATION.html. Then just open it (from you file explorer or directory from ORN SVN website) to be redirected to the DOC.

Example : https://svn.obs-nancay.fr/svn/rh/trunk/Back-end/Temps_reel_belisama/Rack_Piste/Documentation/

### update_changelog.py

This part is optional, this is to auto-create a changelog. It takes commit from SVN log and print it in changelog.md.

